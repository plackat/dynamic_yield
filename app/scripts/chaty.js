var Chat = function () {
    this.name = "chatter";
    this.$el = $('#chat');
}
Chat.prototype.init = function(msg) {
    this.name += msg.id;
    this.update(msg);
};

Chat.prototype.send = function (event, msg) {
    console.log(msg);
    parent.postMessage(msg, '*');
};

Chat.prototype.update = function (msg) {
    this.$el.append('<div><strong>' + msg.name + '</strong>: ' + msg.message + '</div>');
    this.$el[0].scrollTop = this.$el[0].scrollHeight;
    $('#msg').val('');
};

Chat.prototype.receiveMessage = function (event) {
    this[event.data.method](event.data.params);
};

var chat = new Chat();


window.addEventListener("message", function() {
    chat.receiveMessage.apply(chat,arguments);
}, false);

$('#send').on('click', function (event) {
    chat.send(event, {params: {message: $('#msg').val(), name: chat.name}, method: 'publish'});
});

$('#msg').keyup(function (event) {
    if (event.keyCode == 13) {
        chat.send(event, {params: {message: $('#msg').val(), name: chat.name}, method: 'publish'});
    }
    event.preventDefault();
    return false;
});