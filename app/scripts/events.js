var EventsManager = function () {

    var subscriptions = {};

    var on = function (eventName, callback, context) {
        if (!(eventName in subscriptions)) {
            subscriptions[eventName] = [];
        }
        if (typeof(callback) == "string") {
            callback = context[callback];
        }
        subscriptions[eventName].push({
            context: context,
            callback: callback
        });
    };

    var off = function (eventName, callback) {
        if (!(eventName in subscriptions) || !subscriptions[eventName].length) return;
        subscriptions[eventName] = $.map(subscriptions[eventName], function (subscription) {
                if (subscription.callback == callback) {
                    return null;
                } else {
                    return(subscription);
                }
            }
        );
    };

    var trigger = function (eventName, data) {
        var event = {
            type: eventName,
            data: (data || []),
            result: null
        };

        var eventArguments = event.data;
        if (subscriptions[eventName]) {
            subscriptions[eventName].forEach(function (subscription) {
                return subscription.callback.apply(subscription.context, [eventArguments]);
            });
        }
        return event;
    };

    return {
        on: on,
        off: off,
        trigger: trigger
    };
};
