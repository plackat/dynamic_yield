var messages = [];
var chatters = [];
var panelList = '';
var newChat = function (callback) {
    var iframe = document.createElement('iframe');
    iframe.src = '/chaty.html';
    iframe.onload = function () {
//        console.log('iframe loaded');
        callback(iframe.contentWindow);
    };
    var $wrap = $('<li class="panel iframewrap"> </li>');
    $wrap.append(iframe);
    $('.container ul').append($wrap);
    return iframe.contentWindow;
};

var publish = function (msg) {
    chatters.forEach(function (subscriber, sid) {
        console.log('msg is:', msg);
        subscriber.postMessage({method: 'update', params: msg}, '*');
    });
    messages.push(msg);
};
var receiveMessage = function (event) {
    console.log('mainframe received Message', event);
    this[event.data.method](event.data.params);
};

window.addEventListener("message", receiveMessage, false);

$('.addnewchat i').on('click', function () {
    chatters.push(newChat(function (iframe) {
        iframe.postMessage({
            method: 'init',
            params: {
                message: "Welcome to DY Chat World",
                name: 'mainframe',
                id: chatters.length
            }
        }, "*");
    }));
});

/*

$(function () {
    var panelList = $('#draggablePanelList');
    panelList.sortable({
        // Only make the .panel-heading child elements support dragging.
        // Omit this to make then entire <li>...</li> draggable.
//    handle: '.panel-heading',
        update: function () {
            $('.panel', panelList).each(function (index, elem) {
                var $listItem = $(elem),
                    newIndex = $listItem.index();

                // Persist the new indices.
            });
        }
    });
});*/
